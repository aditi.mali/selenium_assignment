package seleniumPracticeAssignment;

import java.awt.Robot;
import java.awt.event.KeyEvent;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;

public class AddLocation {

	public static void main(String[] args) throws Exception {
		// TODO Auto-generated method stub

		
		WebDriver driver= new ChromeDriver();
        Thread.sleep(2000);
        driver.get("https://opensource-demo.orangehrmlive.com");
        driver.manage().window().maximize();
        Thread.sleep(2000);
        driver.findElement(By.xpath("//input[@name='username']")).sendKeys("Admin");
        driver.findElement(By.xpath("//input[@name='password']")).sendKeys("admin123");
        driver.findElement(By.xpath("//button[@type='submit']")).click();
        Thread.sleep(2000);
        driver.findElement(By.xpath("//*[text()='Admin']")).click();
        Thread.sleep(2000);
        driver.findElement(By.xpath("(//*[@class='oxd-topbar-body-nav-tab-item'])[3]")).click();
        WebElement location =driver.findElement(By.xpath("(//*[@class='oxd-topbar-body-nav-tab-link'])[3]"));
        Actions ac=new Actions(driver);
        ac.moveToElement(location).click().perform();
        ac.sendKeys(Keys.ARROW_DOWN).click().perform();
        Thread.sleep(1000);
        ac.sendKeys(Keys.ARROW_DOWN).click().perform();
        driver.findElement(By.xpath("(//*[@type='button'])[4]")).click();
        Thread.sleep(2000);
        
        driver.findElement(By.xpath("(//input[@placeholder='Type here ...'])[1]")).sendKeys("Aditi");
        driver.findElement(By.xpath("(//input[@placeholder='Type here ...'])[2]")).sendKeys("pune");
        driver.findElement(By.xpath("(//input[@placeholder='Type here ...'])[3]")).sendKeys("maharashtra");
        driver.findElement(By.xpath("(//input[@placeholder='Type here ...'])[4]")).sendKeys("416312");
    
        
        Robot r = new Robot();
        driver.findElement(By.xpath("//*[@class='oxd-select-text--after']")).click();
		driver.findElement(By.xpath("//*[contains(text(),'Albania')]")).click();		
		r.keyPress(KeyEvent.VK_DOWN);
		r.keyRelease(KeyEvent.VK_DOWN);
		r.keyPress(KeyEvent.VK_ENTER);
		r.keyRelease(KeyEvent.VK_ENTER);
        driver.findElement(By.xpath("(//input[@placeholder='Type here ...'])[5]")).sendKeys("9965432312");
        driver.findElement(By.xpath("(//input[@placeholder='Type here ...'])[6]")).sendKeys("425001");
        driver.findElement(By.xpath("(//textarea[@placeholder='Type here ...'])[1]")).sendKeys("pune, maharashtra");
        driver.findElement(By.xpath("(//textarea[@placeholder='Type here ...'])[2]")).sendKeys("hiiii");
        Thread.sleep(2000);
        driver.findElement(By.xpath("//button[@type='submit']")).click();
        
        Thread.sleep(2000);
        String text = driver.findElement(By.xpath("//div[text()='Aditi']")).getText();
		System.out.println(text);
		driver.findElement(By.xpath("//i[@class=\"oxd-icon bi-caret-down-fill oxd-userdropdown-icon\"]")).click();
		driver.findElement(By.xpath("//a[text()='Logout']")).click();
        
		
	}

}
