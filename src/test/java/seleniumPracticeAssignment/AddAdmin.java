package seleniumPracticeAssignment;

import java.awt.Robot;
import java.awt.event.KeyEvent;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;

public class AddAdmin {

	public static void main(String[] args) throws Exception 
	{
		// TODO Auto-generated method stub
		
		WebDriver driver= new ChromeDriver();
		Thread.sleep(2000);
		driver.get("https://opensource-demo.orangehrmlive.com");
		driver.manage().window().maximize();
		Thread.sleep(2000);
		driver.findElement(By.xpath("//input[@name='username']")).sendKeys("Admin");
		driver.findElement(By.xpath("//input[@name='password']")).sendKeys("admin123");
		driver.findElement(By.xpath("//button[@type='submit']")).click();
		Thread.sleep(2000);
		driver.findElement(By.xpath("(//li//a[@data-v-4de0f1ff])[1]")).click();
		Thread.sleep(2000);
		driver.findElement(By.xpath("//button[@type='button']//i[@data-v-7e88b27e]")).click();
		Thread.sleep(1000);
		
		driver.findElement(By.xpath("(//i[@class='oxd-icon bi-caret-down-fill oxd-select-text--arrow'])[1]")).click();
		driver.findElement(By.xpath("//*[contains(text(),'Admin')]")).click();
		
		WebElement employee_name = driver.findElement(By.xpath("//input[@placeholder=\"Type for hints...\"]"));
		employee_name.sendKeys("Cecil Bonaparte");		
		Thread.sleep(3000);
		driver.findElement(By.xpath("//*[contains(text(),'Cecil  Bonaparte')]")).click();	
		Thread.sleep(3000);
		Robot r = new Robot();
		driver.findElement(By.xpath("(//i[@class='oxd-icon bi-caret-down-fill oxd-select-text--arrow'])[2]")).click();
		driver.findElement(By.xpath("//*[contains(text(),'Enabled')]")).click();		
		r.keyPress(KeyEvent.VK_DOWN);
		r.keyRelease(KeyEvent.VK_DOWN);
		r.keyPress(KeyEvent.VK_ENTER);
		r.keyRelease(KeyEvent.VK_ENTER);
		
		Thread.sleep(2000);
		driver.findElement(By.xpath("(//input[@data-v-844e87dc])[2]")).sendKeys("aditi");
		driver.findElement(By.xpath("(//input[@data-v-844e87dc])[3]")).sendKeys("Aditi@123");
		driver.findElement(By.xpath("(//input[@data-v-844e87dc])[4]")).sendKeys("Aditi@123");
		Thread.sleep(2000);
		driver.findElement(By.xpath("(//button[@data-v-7e88b27e])[2]")).click();
		
		
		

	}

}
